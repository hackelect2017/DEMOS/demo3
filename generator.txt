Random lightning stroke generator is available on scalar.hackelect.com
on TCP ports:
 - 3003:   1 stroke/s
 - 3002:  10 strokes/s
 - 3001: 100 strokes/s

Server scalar.hackelect.com is dualstacked, accessible via both IPv6
and IPv4 protocols. Usage of IPv6 is encouraged.
